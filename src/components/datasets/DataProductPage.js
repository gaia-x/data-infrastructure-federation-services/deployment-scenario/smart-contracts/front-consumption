import React, { useEffect, useState } from 'react'
import { Container, Grid, Header, Segment, Icon, Divider } from 'semantic-ui-react'
import { gatewayApi } from '../misc/GatewayApi'
import DataProductForm from './DataProductForm'
import DataSetTable from './DataSetTable'
import { isAdmin } from '../misc/Helpers'
import { Navigate } from 'react-router-dom'
import { useKeycloak } from '@react-keycloak/web'
import fileDownload from 'js-file-download'

function DataProductPage() {

  const [dataSetIds, setDataSetIds] = useState([])
  const [dataSet, setDataSet] = useState([])

  const { keycloak } = useKeycloak()

  useEffect(() => {
    handleGetDataProduct();
  }, [])

  const handleGetDataProduct = async() => {
    const response = await gatewayApi.getDataProductDetails(keycloak.token);
    const dataProduct = response.data;
    console.log(`Get all data:  ${JSON.stringify(dataProduct)}`)
    const dataSets = [];
    for(const [key, value] of Object.entries(dataProduct)){
      dataSets.push({ key: key, text: key,  value: key});
    }
    console.log(`Get all datasetID ${JSON.stringify(dataSets)}`);
    setDataSetIds(dataSets);
  }


  const handleSelectDataSetOption = async (datasetOption) => {
    console.log( `User have selected the dataset ${datasetOption}`)
    const response = await gatewayApi.getDataSetDetails(datasetOption, keycloak.token);
    const dataSet = response.data;
    setDataSet(dataSet);
  }

  const handleChange = (event, data) => {
      const selectedDataSet = data.value;
      handleSelectDataSetOption(selectedDataSet); 
  }


  const handleDownloadFile = async (file) => {
    console.log(`File is: ${JSON.stringify(file)}  `)
    const response = await gatewayApi.getFileFromBackend(file.url, keycloak.token);
    fileDownload(response.data, file.filename);
  }

  if (!isAdmin(keycloak)) {
    return <Navigate to='/' />
  }

  return (
    <Container>
      <Grid>
        <Grid.Column mobile={16} tablet={16} computer={4}>
          <Segment>
            <Header as='h2'>
              <Icon name='database' />
              <Header.Content>Data Products</Header.Content>
            </Header>
            <Divider />
            <DataProductForm
              handleChange={handleChange}
              dataSetIds = {dataSetIds}
              handleSelectDataSetOption={handleSelectDataSetOption}
            />
          </Segment>
        </Grid.Column>
        <Grid.Column mobile={16} tablet={16} computer={12}>
          <DataSetTable
            dataSet={dataSet}
            handleDownloadFile= {handleDownloadFile}
          />
        </Grid.Column>
      </Grid>

    </Container>
  )
}

export default DataProductPage