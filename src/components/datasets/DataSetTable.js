import React from 'react'
import { Button, Table, TableCell } from 'semantic-ui-react'

function DataSetTable({  dataSet, handleDownloadFile  }) {
  const height = window.innerHeight - 100
  const style = {
    height: height,
    maxHeight: height,
    overflowY: 'auto',
    overflowX: 'hidden'
  }

  const dataList = dataSet && dataSet.map(element => {
    console.log(`Data of element is ${JSON.stringify(element)}`)
    return (
      <Table.Row key = {element.filename}>
          <TableCell collapsing>
            <Button
              circular
              color='red'
              size='small'
              icon='download'
              //onClick={() => {console.log("User has clicked on the delete button")}}
              onClick={() => {
                handleDownloadFile(element)
              }}
            />
            <Button
             circular
             color='orange'
             size='small'
             icon='eye'
             onClick={() => {console.log("User has clicked on the download button")}}/>
          </TableCell>
          <TableCell>{element.filename}</TableCell>
          <TableCell>{element.url}</TableCell>
      </Table.Row>
    );
  })


  return (
    <div style={style}>
      <Table compact striped>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width={2}/>
            <Table.HeaderCell width={5}>Filename</Table.HeaderCell>
            <Table.HeaderCell width={9}>Download URL</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {dataList}
        </Table.Body>
      </Table>
    </div>
  )
}

export default DataSetTable