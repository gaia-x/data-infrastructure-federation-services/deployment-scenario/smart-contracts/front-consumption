import React, {  useState } from 'react'
import { Container, Grid } from 'semantic-ui-react'

function Home() {
  const [isLoading, setIsLoading] = useState(false)

  return (
    isLoading ? <></> : (
      <Container>
        <Grid centered columns={1} celled>
          <Grid.Column mobile={16} tablet={8} computer={4}>
              <p1>Welcome to Data Exchange Portal</p1>
          </Grid.Column>
        </Grid>
      </Container>
    )
  )
}

export default Home