const prod = {
  url: {
    KEYCLOAK_BASE_URL: "https://keycloak.demo23.gxfs.fr/"
  }
}

const dev = {
  url: {
    KEYCLOAK_BASE_URL: "https://keycloak.demo23.gxfs.fr/"
    //KEYCLOAK_BASE_URL: "http://localhost:8080/"
  }
}

export const config = process.env.NODE_ENV === 'development' ? dev : prod